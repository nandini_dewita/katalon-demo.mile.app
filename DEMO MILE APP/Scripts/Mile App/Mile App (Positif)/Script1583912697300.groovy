import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'membuka halaman browser'
WebUI.openBrowser('')

'membuka halaman "demo.mile.app"'
WebUI.navigateToUrl('https://demo.mile.app/landing')

WebUI.switchToDefaultContent()

'request registrasi demo'
WebUI.click(findTestObject('Object Repository/Page_Mileapp - The best Logistics Platform _f3cbcb/p_Request Demo'))

'input mandatory "nama lengkap"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Full name_name'), 'Nandini Putri Dewita')

'input mandatory "email"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Email_email'), 'dini8dini8@gmail.com')

'input mandatory "nomor telp"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Phone number_phone'), '081286475764')

'input mandatory "nama organisasi"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Company name_organization_name'), 'Teknik Informatika')

WebUI.click(findTestObject('Object Repository/Page_Mileapp  Request Demo/button_Request Demo'))

'menunggu proses kembali ke halaman menu, diberikan timeout'
WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Mileapp - The best Logistics Platform _f3cbcb/a_Login'))

'login menggunakan nama organisasi yang sudah diregistrasi'
WebUI.setText(findTestObject('Page_Mileapp  Login/input_Back to Mile_organization'), 'Teknik Informatika')

WebUI.click(findTestObject('Page_Mileapp  Login/button_Login'))

WebUI.click(findTestObject('Page_Mileapp  Login/a_Contact us'))

WebUI.click(findTestObject('Page_Mileapp  Login/a_Back to Mile'))

'cek menu "Meet the Mile Solutions"'
WebUI.scrollToElement(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h1_Meet the Mile Solutions'), 
    0)

WebUI.delay(2)

'cek menu "Mile Mobile"'
WebUI.scrollToElement(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h1_Mile Mobile'), 
    0)

WebUI.delay(2)

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h2_Versatile Workflow'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h2_Digital Data Collection'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h2_Offline Mode'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/div_four'), 0)

WebUI.delay(2)

'cek menu "Mile Web"'
WebUI.scrollToElement(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h1_Mile Web'), 0)

WebUI.delay(2)

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h2_Real Time Dashboard'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h2_User Management'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/h2_Easy Integration'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'scroll ke bawah untuk melihat selanjutnya'
WebUI.scrollToElement(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/div_five'), 0)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/div_six'), 0)

WebUI.delay(1)

WebUI.closeBrowser()

