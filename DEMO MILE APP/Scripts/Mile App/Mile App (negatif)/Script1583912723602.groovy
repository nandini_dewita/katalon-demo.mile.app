import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'membuka halaman browser'
WebUI.openBrowser('')

'membuka halaman "demo.mile.app"'
WebUI.navigateToUrl('https://demo.mile.app/landing')

WebUI.switchToDefaultContent()

'request registrasi demo'
WebUI.click(findTestObject('Object Repository/Page_Mileapp - The best Logistics Platform _f3cbcb/p_Request Demo'))

'input mandatory "nama lengkap"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Full name_name'), 'Nandini Putri Dewita')

'klik save dengan mengosongkan beberapa mandatory'
WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.delay(5)

'memberikan alert bahwa mandatory belum lengkap'
WebUI.acceptAlert()

'input mandatory "email"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Email_email'), 'dini8dini8@gmail.com')

'klik save dengan mengosongkan beberapa mandatory'
WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.delay(5)

'memberikan alert bahwa mandatory belum lengkap'
WebUI.acceptAlert()

'input mandatory "nomor telp"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Phone number_phone'), '081286475764')

'klik save dengan mengosongkan beberapa mandatory'
WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.delay(5)

'memberikan alert bahwa mandatory belum lengkap'
WebUI.acceptAlert()

'input mandatory "nama organisasi"'
WebUI.setText(findTestObject('Object Repository/Page_Mileapp  Request Demo/input_Company name_organization_name'), 'Teknik Informatika')

'klik save dengan mengosongkan beberapa mandatory'
WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.delay(5)

'memberikan alert bahwa mandatory belum lengkap'
WebUI.acceptAlert()

WebUI.click(findTestObject('Object Repository/Page_Mileapp - The best Logistics Platform _f3cbcb/a_Login'))

'login menggunakan nama organisasi yang sudah diregistrasi'
WebUI.setText(findTestObject('Page_Mileapp  Login/input_Back to Mile_organization'), 'Nandini')

WebUI.click(findTestObject('Page_Mileapp  Login/button_Login'))

'memberikan alert bahwa nama organisasi tidak terdaftar'
WebUI.acceptAlert()

WebUI.closeBrowser()

